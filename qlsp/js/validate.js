function kiemTraTrung(id, dssv) {
  let index = timKiemViTri(id, dssv);
  if (index !== -1) {
    showMessageErr("tbTKNV", "Tài khoản nhân viên đã trùng");
    return false;
  } else {
    showMessageErr("tbTKNV", "");
    return true;
  }
}
function kiemTraRong(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
}
