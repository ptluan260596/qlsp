const BASE_URL = "https://636dac37b567eed48ac63d63.mockapi.io";

let idEdited = null;
// lấy data api và hiện ra màn hình
function fetchAllPhones() {
  batLoading();
  axios({
    url: `${BASE_URL}/phones`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      renderDSSP(res.data);
    })
    .catch(function (err) {
      tatLoading();
    });
}
fetchAllPhones();

///xoá điện thoại
function removePhone(idPhone) {
  batLoading();
  axios({
    url: `${BASE_URL}/phones/${idPhone}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      fetchAllPhones();
    })
    .catch(function (err) {
      tatLoading();
    });
}

//thêm điện thoại
function addPhone() {
  var data = layThongTinTuForm();
  var newPhone = {
    name: data.name,
    price: data.price,
    img: data.img,
    desc: data.desc,
  };
  batLoading();
  axios({
    url: `${BASE_URL}/phones`,
    method: "POST",
    data: newPhone,
  })
    .then(function (res) {
      tatLoading();
      fetchAllPhones();
    })

    .catch(function (err) {
      tatLoading();
    });
}
//sửa thông tin điện thoại
function editPhone(idPhone) {
  axios({
    url: `${BASE_URL}/phones/${idPhone}`,
    method: "GET",
  })
    .then(function (res) {
      document.getElementById("TenSP").value = res.data.name;
      document.getElementById("GiaSP").value = res.data.price;
      document.getElementById("HinhSP").value = res.data.img;
      document.getElementById("MoTa").value = res.data.desc;
      idEdited = res.data.id;
    })
    .catch(function (err) {});
}
//update thông tin điện thoại
function updatePhone() {
  let data = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/phones/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      fetchAllPhones();
    })
    .catch(function (err) {});
}
