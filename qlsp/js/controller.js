function renderDSSP(phones) {
  var contentHTML = "";
  phones.forEach(function (item) {
    var contenTr = `<tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.price}$</td>
    <td><img src="${item.img}" class="w-50 my-auto"/></td>
    <td>${item.desc}</td>
    <td>
    <button class="btn btn-warning" onclick="removePhone(${item.id})">Xoá</button>
    <button class="btn btn-secondary" onclick="editPhone(${item.id})" data-toggle="modal" data-target="#myModal">Sửa</button>
    </td>
    </tr>`;
    contentHTML += contenTr;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var name = document.getElementById("TenSP").value;
  var price = document.getElementById("GiaSP").value;
  var img = document.getElementById("HinhSP").value;
  var desc = document.getElementById("MoTa").value;
  return {
    name: name,
    price: price,
    img: img,
    desc: desc,
  };
}

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
